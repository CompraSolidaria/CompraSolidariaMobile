package br.com.comprasolidaria.comprasolidaria;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import br.com.comprasolidaria.comprasolidaria.util.Utils;
import butterknife.BindView;
import butterknife.ButterKnife;
import static br.com.comprasolidaria.comprasolidaria.util.Constants.*;
public class CampaignsResultsDatesActivity extends AppCompatActivity {

    @BindView(R.id.lv_campaigns_results_dates)
    ListView old_campaigns_dates;

    @BindView(R.id.tv_app_activity_title)
    TextView tv_app_activity_title;

    private SimpleDateFormat viewFormat = new SimpleDateFormat("MMMM (yyyy)");
    private SimpleDateFormat appFormat = new SimpleDateFormat("yyyy/MM");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_campaigns_results_dates);
        ButterKnife.bind(this);
        tv_app_activity_title.setText(getString(R.string.campaigns_results_date_title));

        String[] dates = getListDates(new Date());
        final ArrayAdapter<String> datesAdapter = new ArrayAdapter<>(
                this,
                R.layout.item_campaigns_results_dates,
                R.id.tv_item_campaigns_results_dates,
                dates);

        old_campaigns_dates.setAdapter(datesAdapter);

        old_campaigns_dates.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long rowId) {
                try {
                    Date tempViewDate = viewFormat.parse(datesAdapter.getItem(position));
                    String tempAppDate = appFormat.format(tempViewDate);
                    Intent campaignsResultsMonthIntent = new Intent(getApplicationContext(), CampaignsResultsMonthActivity.class);
                    campaignsResultsMonthIntent.putExtra(CAMPAIGNS_RESULTS_DATE, tempAppDate);

                    if (Utils.checkForConnection(view.getContext())) {
                        startActivity(campaignsResultsMonthIntent);
                    } else {
                        Toast.makeText(view.getContext(), R.string.app_need_connection,Toast.LENGTH_SHORT).show();
                    }
                } catch (ParseException e) {
                    Toast.makeText(getApplicationContext(), R.string.campaigns_results_date_error, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private String[] getListDates (Date date) {
        String[] listDates = new String[CAMPAIGNS_RESULTS_TOTAL_MONTHS];

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        for (int i=1; i <= CAMPAIGNS_RESULTS_TOTAL_MONTHS; i++) {
            if (calendar.MONTH > 0) {
                calendar.add(calendar.MONTH, -1);
                listDates[i-1] = viewFormat.format(calendar.getTime()).toUpperCase();
            } else {
                calendar.add(calendar.MONTH, 12);
                calendar.add(calendar.YEAR, -1);
                listDates[i-1] = viewFormat.format(calendar.getTime()).toUpperCase();
            }
        }
        return listDates;
    }
}
