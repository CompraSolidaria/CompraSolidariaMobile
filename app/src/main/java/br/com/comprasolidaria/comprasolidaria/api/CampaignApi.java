package br.com.comprasolidaria.comprasolidaria.api;

import java.util.List;

import br.com.comprasolidaria.comprasolidaria.model.Campaign;
import br.com.comprasolidaria.comprasolidaria.model.Picture;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by lvinagre on 27/01/2017.
 */

public interface CampaignApi {

    @GET("/campaigns/{id}/pictures")
    Call<List<Picture>> campaignPicturesList(@Path("id") String campaignId);

    @GET("/campaigns/search/votable")
    Call<List<Campaign>> campaignVotableList(@Header("Authorization") String authorization);

    @GET("/campaigns/search/ended/{year}/{month}")
    Call<List<Campaign>> campaignEndedList(@Path("year") String campaignYear, @Path("month") String campaignMonth);

    @POST("/campaigns/vote")
    @FormUrlEncoded
    Call<String> campaignVote(@Header("Authorization") String authorization, @Field("userId") String userId, @Field("campaignId") String campaignId);

    @GET("/campaigns/vote/{user}")
    Call<String> campaignCheckUserVote(@Header("Authorization") String authorization, @Path("user") String userId);

}
