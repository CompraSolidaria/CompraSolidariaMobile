package br.com.comprasolidaria.comprasolidaria;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.tv_app_activity_title)
    TextView tv_app_activity_title;

    @BindView(R.id.activity_login_button)
    LoginButton activity_login_button;

    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        tv_app_activity_title.setText(getString(R.string.login_title));

        callbackManager = CallbackManager.Factory.create();
        activity_login_button.setReadPermissions(Arrays.asList("public_profile","email"));
        activity_login_button.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                finish();
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                try {
                                    if (!object.getString("email").isEmpty()) {
                                        Intent voteIntent = new Intent(getApplicationContext(), CampaignsVoteActivity.class);
                                        startActivity(voteIntent);
                                    } else {
                                        Toast.makeText(getApplicationContext(), R.string.login_fb_email_error, Toast.LENGTH_LONG).show();
                                        AccessToken.setCurrentAccessToken(null);
                                        Profile.setCurrentProfile(null);
                                        finish();
                                    }
                                } catch (JSONException e) {
                                    Toast.makeText(getApplicationContext(), R.string.login_fb_email_error, Toast.LENGTH_LONG).show();
                                    AccessToken.setCurrentAccessToken(null);
                                    Profile.setCurrentProfile(null);
                                    finish();
                                }

                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                finish();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(getApplicationContext(), R.string.login_fb_error, Toast.LENGTH_SHORT);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
