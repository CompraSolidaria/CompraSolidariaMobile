package br.com.comprasolidaria.comprasolidaria.util;

/**
 * Created by lvinagre on 23/01/2017.
 */

public final class Constants {

    public static final int CAMPAIGNS_RESULTS_TOTAL_MONTHS = 5;
    public static final String CAMPAIGNS_RESULTS_DATE = "CampaignsResultsDate";
    public static final String COMPRA_SOLIDARIA_API_URL = "http://192.168.1.44:3000";
    public static final String COMPRA_SOLIDARIA_API_URL_SECURE = "https://genesis:3000";
    public static final String CAMPAIGN_INFO = "CampaignInfo";
    public static final String FORVOTE = "ForVote";
}
