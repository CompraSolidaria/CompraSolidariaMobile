package br.com.comprasolidaria.comprasolidaria;

import android.app.Dialog;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Arrays;

import br.com.comprasolidaria.comprasolidaria.adapter.PictureAdapter;
import br.com.comprasolidaria.comprasolidaria.model.Campaign;
import br.com.comprasolidaria.comprasolidaria.model.Picture;
import br.com.comprasolidaria.comprasolidaria.util.ExpandableHeightGridView;
import br.com.comprasolidaria.comprasolidaria.util.Utils;
import butterknife.BindView;
import butterknife.ButterKnife;

import static br.com.comprasolidaria.comprasolidaria.util.Constants.*;

public class CampaignActivity extends AppCompatActivity {

    @BindView(R.id.tv_app_header)
    TextView tv_app_header;

    @BindView(R.id.tv_app_activity_title)
    TextView tv_app_activity_title;

    @BindView(R.id.iv_campaign)
    ImageView iv_campaign;

    @BindView(R.id.tv_campaign_description)
    TextView tv_campaign_description;

    @BindView(R.id.ll_campaign_pictures_title)
    LinearLayout ll_campaign_pictures_title;

    @BindView(R.id.gv_campaign_pictures)
    ExpandableHeightGridView gv_campaign_pictures;

    private Campaign campaign;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_campaign);
        ButterKnife.bind(this);

        if (getIntent() != null) {

            campaign = getIntent().getExtras().getParcelable(CAMPAIGN_INFO);
            tv_app_header.setText(campaign.getName());

            if (getIntent().getBooleanExtra(FORVOTE, false)) {
                tv_app_activity_title.setText("");
            } else {
                tv_app_activity_title.setText(String.format(getString(R.string.campaign_votes), campaign.getVotes()));
            }

            tv_campaign_description.setText(campaign.getDescription());

            if (campaign.hasPictures() && campaign.getPictures().size() > 0 && Utils.checkForConnection(this)) {

                Picasso.with(this)
                        .load(campaign.getPictures().get(0).getUrl())
                        .placeholder(R.drawable.campaign_card_loader)
                        .into(iv_campaign);

                campaign.getPictures().remove(0);


                if (campaign.getPictures().size() > 0) {
                    gv_campaign_pictures.setAdapter(new PictureAdapter(this, R.layout.item_campaign_pictures, campaign.getPictures()));
                    gv_campaign_pictures.setExpanded(true);
                    gv_campaign_pictures.setFocusable(false);
                    gv_campaign_pictures.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Picture picture = (Picture) parent.getItemAtPosition(position);

                            Dialog pictureDialog = new Dialog(view.getContext(), R.style.MyDialogTheme);
                            pictureDialog.setContentView(R.layout.dialog_campaign_picture);
                            pictureDialog.getWindow().setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                            ImageView iv_dialog_campaign_picture = (ImageView) pictureDialog.findViewById(R.id.iv_dialog_campaign_picture);

                            Picasso.with(view.getContext())
                                    .load(picture.getUrl())
                                    .placeholder(R.drawable.campaign_card_loader)
                                    .into(iv_dialog_campaign_picture);

                            pictureDialog.show();
                        }
                    });
                } else {
                    ll_campaign_pictures_title.setVisibility(View.GONE);
                }
            } else {
                ll_campaign_pictures_title.setVisibility(View.GONE);
            }
        }
    }
}
