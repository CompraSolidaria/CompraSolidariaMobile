package br.com.comprasolidaria.comprasolidaria.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;
import br.com.comprasolidaria.comprasolidaria.R;
import br.com.comprasolidaria.comprasolidaria.listener.OnClickListener;
import br.com.comprasolidaria.comprasolidaria.model.Campaign;
import br.com.comprasolidaria.comprasolidaria.util.Utils;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by lvinagre on 09/02/2017.
 */

public class CampaignVoteAdapter extends RecyclerView.Adapter<CampaignVoteAdapter.CampaignViewHolder> {

    private Context context;
    private List<Campaign> campaignList;
    private boolean userVoted;
    private OnClickListener voteClickListener, viewClickListener;

    public CampaignVoteAdapter(Context context, List<Campaign> campaignList, boolean userVoted, OnClickListener voteClickListener, OnClickListener viewClickListener) {
        this.context = context;
        this.campaignList = campaignList;
        this.userVoted = userVoted;
        this.voteClickListener = voteClickListener;
        this.viewClickListener = viewClickListener;
    }

    @Override
    public CampaignViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.item_campaigns_vote_card, parent, false);
        return new CampaignViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CampaignViewHolder holder, int position) {
        holder.tv_item_campaigns_vote_card_name.setText(campaignList.get(position).getName());

        if (campaignList.get(position).hasPictures() &&
                campaignList.get(position).getPictures().size() > 0 &&
                Utils.checkForConnection(context)) {

            Picasso.with(context)
                    .load(campaignList.get(position).getPictures().get(0).getUrl())
                    .placeholder(R.drawable.campaign_card_loader)
                    .into(holder.iv_item_campaigns_vote_card);
        } else {
            holder.iv_item_campaigns_vote_card.setImageResource(R.drawable.campaign_card_loader);
        }

        if (!userVoted) {
            holder.btn_item_campaigns_vote_vote.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    voteClickListener.onClick(holder.itemView, holder.getAdapterPosition());
                }
            });
        } else {
            holder.btn_item_campaigns_vote_vote.setVisibility(View.INVISIBLE);
        }

        holder.btn_item_campaigns_vote_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewClickListener.onClick(holder.itemView, holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return campaignList.size();
    }

    public Campaign getItem(int position) {return campaignList.get(position);}

    public void userVoted() {
        this.userVoted = true;
    }

    static class CampaignViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_item_campaigns_vote_card)
        ImageView iv_item_campaigns_vote_card;

        @BindView(R.id.tv_item_campaigns_vote_card_name)
        TextView tv_item_campaigns_vote_card_name;

        @BindView(R.id.btn_item_campaigns_vote_vote)
        ImageView btn_item_campaigns_vote_vote;

        @BindView(R.id.btn_item_campaigns_vote_view)
        ImageView btn_item_campaigns_vote_view;

        public CampaignViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
