package br.com.comprasolidaria.comprasolidaria;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.Profile;
import com.facebook.login.LoginManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.comprasolidaria.comprasolidaria.adapter.CampaignVoteAdapter;
import br.com.comprasolidaria.comprasolidaria.api.CampaignApi;
import br.com.comprasolidaria.comprasolidaria.listener.OnClickListener;
import br.com.comprasolidaria.comprasolidaria.model.Campaign;
import br.com.comprasolidaria.comprasolidaria.model.Picture;
import br.com.comprasolidaria.comprasolidaria.util.Utils;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static br.com.comprasolidaria.comprasolidaria.util.Constants.*;

public class CampaignsVoteActivity extends AppCompatActivity implements Callback<List<Campaign>> {

    @BindView(R.id.tv_app_activity_title)
    TextView tv_app_activity_title;

    @BindView(R.id.rv_campaign_vote)
    RecyclerView rv_campaign_vote;

    private SimpleDateFormat appFormat = new SimpleDateFormat("yyyy/MM");
    private CampaignVoteAdapter campaignVoteAdapter;
    private CampaignApi campaignApi;
    private ProgressDialog progress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_campaigns_vote);
        ButterKnife.bind(this);

        String currentDate = appFormat.format(new Date());
        tv_app_activity_title.setText(getString(R.string.campaigns_vote_title) + " " + currentDate);

        rv_campaign_vote.setLayoutManager(new LinearLayoutManager(this));
        rv_campaign_vote.setItemAnimator(new DefaultItemAnimator());
        rv_campaign_vote.setHasFixedSize(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(COMPRA_SOLIDARIA_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        campaignApi = retrofit.create(CampaignApi.class);

        progress = new ProgressDialog(this);
        progress.setIndeterminate(true);
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setMessage(getString(R.string.progress_dialog_message));
        progress.setCancelable(false);
        progress.show();

        loadCampaignsList();
    }

    private void loadCampaignsList() {
        Call<List<Campaign>> call = campaignApi.campaignVotableList(AccessToken.getCurrentAccessToken().getToken());
        call.enqueue(this);
    }

    private OnClickListener viewClickListener() {
        return new OnClickListener() {
            @Override
            public void onClick(View view, int position) {
                if (Utils.checkForConnection(view.getContext())) {
                    Intent campaignInfoIntent = new Intent(view.getContext(), CampaignActivity.class);
                    campaignInfoIntent.putExtra(CAMPAIGN_INFO, campaignVoteAdapter.getItem(position));
                    campaignInfoIntent.putExtra(FORVOTE, true);
                    startActivity(campaignInfoIntent);
                } else {
                    Toast.makeText(view.getContext(), R.string.app_need_connection,Toast.LENGTH_SHORT).show();
                }
            }
        };
    }

    private OnClickListener voteClickListener() {
        return new OnClickListener() {
            @Override
            public void onClick(View view, int position) {
                if (Utils.checkForConnection(view.getContext())) {
                    Call<String> call = campaignApi.campaignVote(
                            AccessToken.getCurrentAccessToken().getToken(),
                            AccessToken.getCurrentAccessToken().getUserId(),
                            campaignVoteAdapter.getItem(position).getId());
                    call.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            if (response.code() == 200) {
                                Toast.makeText(getApplicationContext(), R.string.cs_backend_campaign_vote_success,Toast.LENGTH_SHORT).show();
                                campaignVoteAdapter.userVoted();
                                campaignVoteAdapter.notifyDataSetChanged();
                            } else {
                                Toast.makeText(getApplicationContext(), R.string.cs_backend_campaign_vote_error,Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), R.string.cs_backend_campaign_vote_error,Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    Toast.makeText(view.getContext(), R.string.app_need_connection,Toast.LENGTH_SHORT).show();
                }
            }
        };
    }

    @Override
    public void onResponse(Call<List<Campaign>> call, final Response<List<Campaign>> response) {
        if (response.isSuccessful()) {
            final Context tempContext = this;
            if (response.body().isEmpty()) {
                Toast.makeText(getApplicationContext(), R.string.cs_backend_campaigns_no_data, Toast.LENGTH_SHORT).show();
                progress.dismiss();
                finish();
            }

            Call<String> voteCall = campaignApi.campaignCheckUserVote(AccessToken.getCurrentAccessToken().getToken(), AccessToken.getCurrentAccessToken().getUserId());
            voteCall.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> voteResponse) {
                    if (voteResponse.isSuccessful()) {
                        if (voteResponse.body().equals("0")) {
                            campaignVoteAdapter = new CampaignVoteAdapter(tempContext, response.body(), false, voteClickListener(), viewClickListener());
                        } else {
                            campaignVoteAdapter = new CampaignVoteAdapter(tempContext, response.body(), true, voteClickListener(), viewClickListener());
                            Toast.makeText(getApplicationContext(), R.string.campaigns_vote_voted, Toast.LENGTH_SHORT).show();
                        }
                    }

                    for (int i = 0; i < campaignVoteAdapter.getItemCount(); i++) {
                        final int c = i;
                        Call<List<Picture>> picCall = campaignApi.campaignPicturesList(campaignVoteAdapter.getItem(c).getId());
                        picCall.enqueue(new Callback<List<Picture>>() {
                            @Override
                            public void onResponse(Call<List<Picture>> call, Response<List<Picture>> picResponse) {
                                if (picResponse.isSuccessful()) {
                                    if (picResponse.body().isEmpty()) {
                                        Toast.makeText(getApplicationContext(), String.format(getString(R.string.cs_backend_pictures_no_data), campaignVoteAdapter.getItem(c).getName()), Toast.LENGTH_SHORT).show();
                                        campaignVoteAdapter.getItem(c).setPictures(new ArrayList<Picture>());
                                    } else {
                                        campaignVoteAdapter.getItem(c).setPictures(picResponse.body());
                                    }
                                    rv_campaign_vote.swapAdapter(campaignVoteAdapter, true);
                                    progress.dismiss();
                                } else {
                                    Toast.makeText(getApplicationContext(), String.format(getString(R.string.cs_backend_pictures_bad_response), campaignVoteAdapter.getItem(c).getName()), Toast.LENGTH_SHORT).show();
                                    campaignVoteAdapter.getItem(c).setPictures(new ArrayList<Picture>());
                                    progress.dismiss();
                                }
                            }

                            @Override
                            public void onFailure(Call<List<Picture>> call, Throwable t) {
                                Toast.makeText(getApplicationContext(), String.format(getString(R.string.cs_backend_pictures_connection_error), campaignVoteAdapter.getItem(c).getName()), Toast.LENGTH_SHORT).show();
                                campaignVoteAdapter.getItem(c).setPictures(new ArrayList<Picture>());
                                progress.dismiss();
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), R.string.cs_backend_campaigns_connection_error, Toast.LENGTH_SHORT).show();
                    progress.dismiss();
                    finish();
                }
            });
        }
    }

    @Override
    public void onFailure(Call<List<Campaign>> call, Throwable t) {
        Toast.makeText(getApplicationContext(), R.string.cs_backend_campaigns_connection_error, Toast.LENGTH_SHORT).show();
        progress.dismiss();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AccessToken.setCurrentAccessToken(null);
        Profile.setCurrentProfile(null);
    }
}
