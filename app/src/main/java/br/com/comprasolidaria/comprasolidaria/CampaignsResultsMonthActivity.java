package br.com.comprasolidaria.comprasolidaria;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.comprasolidaria.comprasolidaria.adapter.CampaignResultAdapter;
import br.com.comprasolidaria.comprasolidaria.api.CampaignApi;
import br.com.comprasolidaria.comprasolidaria.listener.OnClickListener;
import br.com.comprasolidaria.comprasolidaria.model.Campaign;
import br.com.comprasolidaria.comprasolidaria.model.Picture;
import br.com.comprasolidaria.comprasolidaria.util.Utils;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static br.com.comprasolidaria.comprasolidaria.util.Constants.*;

public class CampaignsResultsMonthActivity extends AppCompatActivity implements Callback<List<Campaign>> {

    @BindView(R.id.tv_app_activity_title)
    TextView tv_app_activity_title;

    @BindView(R.id.rv_campaigns_results_month)
    RecyclerView rv_campaigns_results_month;

    private String campaignsDate;
    private CampaignResultAdapter campaignResultAdapter;
    private CampaignApi campaignApi;
    private ProgressDialog progress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_campaigns_results_month);
        ButterKnife.bind(this);

        if(getIntent() != null) {
            campaignsDate = getIntent().getExtras().getString(CAMPAIGNS_RESULTS_DATE);
        }

        tv_app_activity_title.setText(getString(R.string.campaigns_results_month_title) + " " + campaignsDate);

        rv_campaigns_results_month.setLayoutManager(new LinearLayoutManager(this));
        rv_campaigns_results_month.setItemAnimator(new DefaultItemAnimator());
        rv_campaigns_results_month.setHasFixedSize(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(COMPRA_SOLIDARIA_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        campaignApi = retrofit.create(CampaignApi.class);

        progress = new ProgressDialog(this);
        progress.setIndeterminate(true);
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setMessage(getString(R.string.progress_dialog_message));
        progress.setCancelable(false);
        progress.show();

        loadCampaignsList();
    }

    private void loadCampaignsList() {
        Call<List<Campaign>> call = campaignApi.campaignEndedList(campaignsDate.split("/")[0], campaignsDate.split("/")[1]);
        call.enqueue(this);
    }

    private OnClickListener campaignClickListener(){
        return new OnClickListener() {
            @Override
            public void onClick(View view, int position) {
                if (Utils.checkForConnection(view.getContext())) {
                    Intent campaignInfoIntent = new Intent(view.getContext(), CampaignActivity.class);
                    campaignInfoIntent.putExtra(CAMPAIGN_INFO, campaignResultAdapter.getItem(position));
                    campaignInfoIntent.putExtra(FORVOTE, false);
                    startActivity(campaignInfoIntent);
                } else {
                    Toast.makeText(view.getContext(), R.string.app_need_connection,Toast.LENGTH_SHORT).show();
                }
            }
        };
    }

    @Override
    public void onResponse(Call<List<Campaign>> call, Response<List<Campaign>> response) {
        if (response.isSuccessful()) {

            if (response.body().isEmpty()) {
                Toast.makeText(getApplicationContext(), R.string.cs_backend_campaigns_no_data, Toast.LENGTH_SHORT).show();
                progress.dismiss();
                finish();
            }

            campaignResultAdapter = new CampaignResultAdapter(this, response.body(), campaignClickListener());

            for (int i = 0; i < campaignResultAdapter.getItemCount(); i++) {
                final int c = i;
                Call<List<Picture>> picCall = campaignApi.campaignPicturesList(campaignResultAdapter.getItem(c).getId());
                picCall.enqueue(new Callback<List<Picture>>() {
                    @Override
                    public void onResponse(Call<List<Picture>> call, Response<List<Picture>> response) {
                        if (response.isSuccessful()) {
                            if (response.body().isEmpty()) {
                                Toast.makeText(getApplicationContext(), String.format(getString(R.string.cs_backend_pictures_no_data), campaignResultAdapter.getItem(c).getName()), Toast.LENGTH_SHORT).show();
                                campaignResultAdapter.getItem(c).setPictures(new ArrayList<Picture>());
                            } else {
                                campaignResultAdapter.getItem(c).setPictures(response.body());
                            }
                            rv_campaigns_results_month.swapAdapter(campaignResultAdapter, true);
                            progress.dismiss();
                        } else {
                            Toast.makeText(getApplicationContext(), String.format(getString(R.string.cs_backend_pictures_bad_response), campaignResultAdapter.getItem(c).getName()), Toast.LENGTH_SHORT).show();
                            campaignResultAdapter.getItem(c).setPictures(new ArrayList<Picture>());
                            progress.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Picture>> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), String.format(getString(R.string.cs_backend_pictures_connection_error), campaignResultAdapter.getItem(c).getName()), Toast.LENGTH_SHORT).show();
                        campaignResultAdapter.getItem(c).setPictures(new ArrayList<Picture>());
                        progress.dismiss();
                    }
                });
            }
        } else {
            Toast.makeText(getApplicationContext(), R.string.cs_backend_campaigns_bad_response, Toast.LENGTH_SHORT).show();
            progress.dismiss();
            finish();
        }
    }

    @Override
    public void onFailure(Call<List<Campaign>> call, Throwable t) {
        Toast.makeText(getApplicationContext(), R.string.cs_backend_campaigns_connection_error, Toast.LENGTH_SHORT).show();
        progress.dismiss();
        finish();
    }
}
