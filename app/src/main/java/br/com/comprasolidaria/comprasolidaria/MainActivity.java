package br.com.comprasolidaria.comprasolidaria;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.Profile;

import br.com.comprasolidaria.comprasolidaria.util.Utils;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    private ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        progress = new ProgressDialog(this);
        progress.setIndeterminate(true);
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setMessage(getString(R.string.progress_dialog_message));
        progress.setCancelable(false);
    }

    @OnClick(R.id.rl_campaigns_vote)
    public void vote() {
        progress.show();
        if (Utils.checkForConnection(this)) {
            if (AccessToken.getCurrentAccessToken() == null || AccessToken.getCurrentAccessToken().isExpired()) {
                Intent loginIntent = new Intent(this, LoginActivity.class);
                startActivity(loginIntent);
                progress.dismiss();
            } else {
                Intent voteIntent = new Intent(getApplicationContext(), CampaignsVoteActivity.class);
                startActivity(voteIntent);
                progress.dismiss();
            }
        } else {
            Toast.makeText(this, R.string.app_need_connection,Toast.LENGTH_SHORT).show();
            progress.dismiss();
        }
    }

    @OnClick(R.id.rl_campaigns_results)
    public void results() {
        if (Utils.checkForConnection(this)) {
            startActivity(new Intent(this, CampaignsResultsDatesActivity.class));
        } else {
            Toast.makeText(this, R.string.app_need_connection,Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        AccessToken.setCurrentAccessToken(null);
        Profile.setCurrentProfile(null);
    }
}
