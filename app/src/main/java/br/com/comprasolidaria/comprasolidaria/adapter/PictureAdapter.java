package br.com.comprasolidaria.comprasolidaria.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
;
import java.util.List;

import br.com.comprasolidaria.comprasolidaria.R;
import br.com.comprasolidaria.comprasolidaria.model.Picture;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by lvinagre on 03/02/2017.
 */

public class PictureAdapter extends ArrayAdapter<Picture> {
    private Context context;
    private int layoutResourceId;
    private List<Picture> pictures;


    public PictureAdapter(Context context, int layoutResourceId, List<Picture> pictures) {
        super(context, layoutResourceId, pictures);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.pictures = pictures;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Picasso.with(context)
                .load(pictures.get(position).getUrl())
                .fit()
                .placeholder(R.drawable.campaign_card_loader)
                .into(holder.iv_campaign_picture);

        return convertView;

    }

    public Picture getItem(int position) {
        return pictures.get(position);
    }

    static class ViewHolder {
        @BindView(R.id.iv_campaign_picture)
        ImageView iv_campaign_picture;

        public ViewHolder(View itemView) {
            ButterKnife.bind(this, itemView);
        }
    }
}
