package br.com.comprasolidaria.comprasolidaria.listener;

import android.view.View;

/**
 * Created by lvinagre on 27/01/2017.
 */

public interface OnClickListener {

    void onClick(View v, int position);
}